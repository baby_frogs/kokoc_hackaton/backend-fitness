import datetime
import os
from fastapi import FastAPI
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow

start_date = datetime.datetime(2023, 9, 1)
end_date = datetime.datetime(2023, 10, 1)
start_time_millis = int(start_date.timestamp()) * 1000
end_time_millis = int(end_date.timestamp()) * 1000


def login(creds):
    SCOPES = ['https://www.googleapis.com/auth/fitness.activity.read']

    if os.path.exists('../token.json'):
        creds = Credentials.from_authorized_user_file('../token.json', SCOPES)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '../credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('../token.json', 'w') as token:
            token.write(creds.to_json())
    return creds


def package_data(response, value):
    data = []
    for bucket in response['bucket']:
        if bucket['dataset'][0]['point']:
            unit = bucket['dataset'][0]['point'][0]['value'][0][value]
        else:
            continue
        start_time = datetime.datetime.fromtimestamp(int(bucket['startTimeMillis']) / 1000)
        data.append({"date": start_time.strftime('%Y-%m-%d'), "value": unit})
    return data


def get_steps(creds):
    creds = Credentials.from_authorized_user_info(creds, scopes=['https://www.googleapis.com/auth/fitness.activity.read'])
    fit_service = build('fitness', 'v1', credentials=creds)
    request = fit_service.users().dataset().aggregate(
        userId='me',
        body={
            "aggregateBy": [{
                "dataSourceId":
                    "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps"
            }],
            "bucketByTime": {
                "durationMillis": 86400000
            },
            "startTimeMillis": str(start_time_millis),
            "endTimeMillis": str(end_time_millis)
        }
    )
    response = request.execute()
    return package_data(response, "intVal")


def get_calories(creds):
    creds = Credentials.from_authorized_user_info(creds, scopes=['https://www.googleapis.com/auth/fitness.activity.read'])
    fit_service = build('fitness', 'v1', credentials=creds)
    request = fit_service.users().dataset().aggregate(
        userId='me',
        body={
            "aggregateBy": [{
                "dataTypeName": "com.google.calories.expended"
            }],
            "bucketByTime": {
                "durationMillis": 86400000
            },
            "startTimeMillis": str(start_time_millis),
            "endTimeMillis": str(end_time_millis)
        }
    )
    response = request.execute()
    return package_data(response, "fpVal")


def get_minutes(creds):
    creds = Credentials.from_authorized_user_info(creds, scopes=['https://www.googleapis.com/auth/fitness.activity.read'])
    fit_service = build('fitness', 'v1', credentials=creds)
    request = fit_service.users().dataset().aggregate(
        userId='me',
        body={
            "aggregateBy": [{
                "dataTypeName": "com.google.active_minutes"
            }],
            "bucketByTime": {
                "durationMillis": 86400000
            },
            "startTimeMillis": str(start_time_millis),
            "endTimeMillis": str(end_time_millis)
        }
    )
    response = request.execute()
    return package_data(response, "intVal")


def get_activity(creds):
    creds = Credentials.from_authorized_user_info(creds, scopes=['https://www.googleapis.com/auth/fitness.activity.read'])
    fit_service = build('fitness', 'v1', credentials=creds)
    activity_type = "com.google.activity.segment"

    request = fit_service.users().dataset().aggregate(
        userId='me',
        body={
            "aggregateBy": [{
                "dataTypeName": "com.google.activity.segment"
            }],
            "bucketByActivitySegment": {
                "minDurationMillis": 300000
            },
            "startTimeMillis": str(start_time_millis),
            "endTimeMillis": str(end_time_millis),
            "filterByActivityType": [activity_type]
        }
    )

    response = request.execute()
    data = []
    for bucket in response['bucket']:
        activity_name = bucket['dataset'][0]['point'][0]['value'][0]['intVal']
        duration = bucket['dataset'][0]['point'][0]['value'][1]['intVal'] / 1000
        start_time = datetime.datetime.fromtimestamp(int(bucket['startTimeMillis']) / 1000)
        end_time = datetime.datetime.fromtimestamp(int(bucket['endTimeMillis']) / 1000)
        data.append({"activity_number": activity_name, "start_time": start_time.strftime('%Y-%m-%d %H:%M:%S'), "end_time": end_time.strftime('%Y-%m-%d %H:%M:%S'),
                     "duration": duration / 60})
        # print(f"Тренировка ходьбы: {activity_name}")
        # print(f"Начало: {start_time.strftime('%Y-%m-%d %H:%M:%S')}")
        # print(f"Окончание: {end_time.strftime('%Y-%m-%d %H:%M:%S')}")
        # print(f"Продолжительность: {duration / 60} минут\n")
    return data


app = FastAPI()


@app.get("/")
async def read_root():
    return {"Hello": "World"}


@app.post("/calories/")
async def read_item(token: dict):
    return get_calories(token)


@app.post("/minutes/")
async def read_item(token: dict):
    return get_minutes(token)


@app.post("/steps/")
async def read_item(token: dict):
    return get_steps(token)


@app.post("/activity/")
async def read_item(token: dict):
    return get_activity(token)
